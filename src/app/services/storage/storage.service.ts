import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";
@Injectable({
  providedIn: "root",
})
export class StorageService {

  private storage = new BehaviorSubject<string>(null);
  private user = new BehaviorSubject<string>(null);
  private token = new BehaviorSubject<string>(null);

  constructor(private _storage: Storage) {}

  watch(): Observable<any> {
    return this.storage.asObservable();
  }

  watchUser(): Observable<any> {
    return this.user.asObservable();
  }



  /**
   * Setear en el storage
   * los datos del usuario
   */

  setUser(user: any): void {
    this._storage.set("user", user);
    this.user.next(user);
  }

  /**
   * Borra al
   * usuario
   */
  removeUser(): void {
    this._storage.remove("user");
    this.user.next(null);
  }

  /**
   * Retorna los datos del usuario
   * @returns
   */
  async getUser() {
    return await this._storage.get("user");
  }

  /**
   * Setear en el storage
   * los datos del token
   */

  setToken(token: any): void {
    this._storage.set("token", token);
    this.token.next(token);
  }

  /**
   * Borra al
   * token
   */
  removeToken(): void {
    this._storage.remove("token");
    this.token.next(null);
  }

  /**
   * Retorna los datos del token
   * @returns
   */
  async getToken() {
    return await this._storage.get("token");
  }
}
