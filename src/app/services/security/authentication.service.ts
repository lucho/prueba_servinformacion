import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { environment } from "../../../environments/environment";
import { StorageService } from "src/app/services/storage/storage.service";
import { User } from "src/app/interfaces/user";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {

  private token: string = null;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private _storageService: StorageService
  ) {}

  
  /**
   * @description Tiene como objetivo iniciar sesion en el sistema
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param User
   * @returns boolean
   */
  login(user:User): Promise<boolean> {
    const authData = {
      ...user,
      returnSecureToken: true
    };

    return new Promise((resolve) => {
      this.http.post(`${environment.urlFirebase}/verifyPassword?key=${ environment.apikey }`, authData).subscribe(
        (response: any) => {
          if (response.idToken) {
             this.setUserData(response);
            resolve(true);
          } else {
            this.storage.clear();
            resolve(false);
          }
        },
        (err) => {
          this.storage.clear();
          resolve(false);
        }
      );
    });
  }



 
   /**
   * @description Tiene como objetivo registrar al usaurio en firebase
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param User
   * @returns boolean
   */
  registerUser(user: User): Promise<boolean> {
    const authData = {
      ...user,
      returnSecureToken: true
    };

    return new Promise((resolve) => {
      this.http.post(`${ environment.urlFirebase }/signupNewUser?key=${ environment.apikey }`, authData).subscribe(
        (response:any) => {
          if (response.idToken) {
           this.setUserData(response);
            resolve(true);
          } else {
            this.storage.clear();
            resolve(false);
          }
          resolve(true);
        },
        (err) => {
           this.storage.clear();
          resolve(false);
        }
      );
    });
  }

 

   /**
   * @description Tiene como objetivo validar si existe el token de sesión del usuario
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @returns boolean
   */
  async validateToken(): Promise<boolean> {
    await this.loadToken();

    if (!this.token) {
      return Promise.resolve(false);
    }
    return Promise.resolve(true);
  }


  /**
   * @description Tiene como objetivo cargar en la variable global de la clase el token del storage
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @returns string
   */
  async loadToken() {
    this.token = await this.storage.get("token");
  }

  /**
   * @description Tiene como objetivo setear los datos del usaurio en el storage
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param User
   * @returns boolean
   */
  setUserData(response:any){
    this._storageService.setUser({'email': response.email});
    this._storageService.setToken(response.idToken);
  }  
}
