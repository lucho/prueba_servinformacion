import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class UsersService {
  constructor(private http: HttpClient) {}

  /**
   * @description Tiene como objetivo consultar los usuarios por nombre
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param term
   * @returns any
   */
  async getSearchUsers(term: string): Promise<any> {
    const response: any = await this.http.get(`${environment.url}/search/users?q=${term}`).toPromise();
    return response;
  }


   /**
   * @description Tiene como objetivo consultar el usuario por su login
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param name
   * @returns any
   */
	 async getInformationProfile(name:string): Promise<any> {
     console.log(`${environment.url}/users/${name}`);
		const response: any = await this.http.get(`${environment.url}/users/${name}`).toPromise();
		return response;
	}

  /**
   * @description Tiene como objetivo consultar el número de seguidores de un usuario
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param name
   * @returns any
   */
  async getUserFollowers(name:string): Promise<any> {
    console.log(`${environment.url}/users/${name}`);
   const response: any = await this.http.get(`${environment.url}/users/${name}/followers`).toPromise();
   return response;
  }

  /**
   * @description Tiene como objetivo consultar el número de suscriptores de un usuario
   * @author Luis Garizabalo <luisf7ll28@gmail.com>
   * @date 2021-10-23
   * @param name
   * @returns any
   */
   async getUserSubscriptions(name:string): Promise<any> {
    console.log(`${environment.url}/users/${name}`);
   const response: any = await this.http.get(`${environment.url}/users/${name}/subscriptions`).toPromise();
   return response;
  }
  
}
