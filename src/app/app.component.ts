import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UtilService } from './util.service';
import { menuController } from '@ionic/core';
import { Router } from '@angular/router';
import { StorageService } from './services/storage/storage.service';
import { debounceTime } from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public user: any = null;

  public isMenuEnabled:boolean = false;
  public selectedIndex = 0;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private util: UtilService,
    private router: Router,
    private _storageServices:StorageService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async ngOnInit() {
    this.user = await this._storageServices.getUser();
if(this.user){
  this.isMenuEnabled = true;
}
    this.selectedIndex = 1;
    
    // this.util.getMenuState().subscribe(menuState => {
    //   this.isMenuEnabled = menuState;
    // });

    this._storageServices
      .watchUser()
      .pipe(debounceTime(500))
      .subscribe((data: any) => {
        if (data) {
          this.isMenuEnabled = true;
        }
      });
  }

  navigate(path, selectedId) {
    this.selectedIndex = selectedId;
    this.router.navigate([path]);
  }

  close() {
    menuController.toggle();
  }
}
