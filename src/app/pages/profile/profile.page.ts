import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { Profile } from "../../interfaces/profile";
import { UiServiceService } from "../../services/ui/ui-service.service";
import { ToastController, NavController } from "@ionic/angular";
import { NgForm } from "@angular/forms";
import { AuthenticationService } from "../../services/security/authentication.service";
import { StorageService } from "../../services/storage/storage.service";
import { ActivatedRoute } from "@angular/router";
import { UsersService } from "src/app/services/github/users.service";
import { Chart, registerables } from "chart.js";
Chart.register(...registerables);
@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  // Importing ViewChild. We need @ViewChild decorator to get a reference to the local variable
  // that we have added to the canvas element in the HTML template.
  @ViewChild("barCanvas") private barCanvas: ElementRef;
  @ViewChild("doughnutCanvas") private doughnutCanvas: ElementRef;
  @ViewChild("lineCanvas") private lineCanvas: ElementRef;

  private userName: string;
  private profile: Profile = { avatar_url: "", name: "---", id: "" };
  private followers: any = [];
  private subscriptions: any = [];

  private barChart: any;
  private doughnutChart: any;
  constructor(
    private authenticationService: AuthenticationService,
    private uiService: UiServiceService,
    private navCtrl: NavController,
    private _storageService: StorageService,
    private _uiServices: UiServiceService,
    private _toastController: ToastController,
    private _route: ActivatedRoute,
    private _usersService: UsersService
  ) {}

  async ngOnInit() {
    this.userName = this._route.snapshot.paramMap.get("name");
    if (this.userName.length < 4 || this.userName == "gcpglobal") {
      this.uiService.alertaInformativa(
        `El texto del usuario no es valido`,
        `Error`,
        `texto: ${this.userName}`
      );
      this.navCtrl.navigateRoot(`home`, {
        animated: true,
        animationDirection: "forward",
      });
    }
    this.profile = await this._usersService.getInformationProfile( this.userName);
    const responseFollowers: any = await this._usersService.getUserFollowers(this.userName);
    const responseSubscriptionss: any = await this._usersService.getUserSubscriptions(this.userName);
    this.followers = responseFollowers.length;
    this.subscriptions = responseSubscriptionss.length;
    await this.doughnutChartMethod();
  }

 

  doughnutChartMethod() {
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      data: {
        labels: ["Followers", "Subscriptions"],
        datasets: [
          {
            label: "# of Votes",
            data: [this.followers, this.subscriptions],
            backgroundColor: [
              "rgba(255, 159, 64, 0.2)",
              "rgba(255, 99, 132, 0.2)",
            ],
            hoverBackgroundColor: ["#FFCE56", "#FF6384"],
          },
        ],
      },
    });
  }
}
