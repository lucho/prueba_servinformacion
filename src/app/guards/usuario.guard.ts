import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/security/authentication.service';
import { NavController } from '@ionic/angular';
import { UiServiceService } from '../services/ui/ui-service.service';
@Injectable({
  providedIn: 'root'
})
export class UsuarioGuard implements  CanLoad {

 constructor( private authenticationService: AuthenticationService,
  private navCtrl : NavController,
  private _uiServiceService:UiServiceService){}

  canLoad(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return  this.authenticationService.validateToken().then( isAuthenticated => {
      if (!isAuthenticated) {
        this.navCtrl.navigateRoot(`welcome`,{ animated: true,animationDirection: 'forward'});
        this._uiServiceService.alertaInformativa(`Tiene que iniciar sesión`,`Error`,`Usuario no logueado`);
        return false;
      } else {
        return true;
      }
    }).catch( error => {
      this.navCtrl.navigateRoot(`welcome`,{ animated: true,animationDirection: 'forward'});
      return false;
    });
    
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return  this.authenticationService.validateToken().then( isAuthenticated => {
      if (!isAuthenticated) {
        this.navCtrl.navigateRoot(`welcome`,{ animated: true,animationDirection: 'forward'});
        this._uiServiceService.alertaInformativa(`Tiene que iniciar sesión`,`Error`,`Usuario no logueado`);
        return false;
      } else {
        return true;
      }
    }).catch( error => {
      this.navCtrl.navigateRoot(`welcome`,{ animated: true,animationDirection: 'forward'});
      return false;
    });
  }
  
}
